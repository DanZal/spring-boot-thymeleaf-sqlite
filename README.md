# Spring Boot Application 

# Chinook SQL lite data base added 

API endpoints:

- **api/customers** (all customers in database)
- **api/customer/id** (customer data based on id)
- **api/customer/name** (customer data based on name)
- **api/customer/country** (number of customers in each country )
- **api/customer/spender** ( highest spending customers)


Thymeleaf endpoints:

- **/** (homepage)
- **/tracks**  (search for tracks)


Search function currently only finds tracks if searched string is exact match. 

