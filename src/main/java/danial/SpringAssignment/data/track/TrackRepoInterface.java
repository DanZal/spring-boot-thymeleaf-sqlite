package danial.SpringAssignment.data.track;

import danial.SpringAssignment.models.track.Tracks;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

public interface TrackRepoInterface {
    public ArrayList<Tracks> getRandomTracks();
    ArrayList<Tracks> findByKeyword(@RequestParam("keyword") String keyword);
    Tracks getTrackByName(String name);

}
