package danial.SpringAssignment.data.track;

import danial.SpringAssignment.data.ConnectionHelper;
import danial.SpringAssignment.models.track.Tracks;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepo implements TrackRepoInterface {

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection connection = null;

    public TrackRepo() {}

    @Override
    public ArrayList<Tracks> getRandomTracks() {
        ArrayList<Tracks> tracks = new ArrayList<>();
        try{

            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT * FROM Track ORDER BY random() Limit 5");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(
                        new Tracks(
                                resultSet.getString("name"),
                                resultSet.getString("composer")
                        ));
            }
            System.out.println("Select 5 random tracks successful");
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return tracks;
    }

    @Override
    public ArrayList<Tracks> findByKeyword(String keyword) {
        ArrayList<Tracks> tracks = new ArrayList<>();
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT Name, Composer from Track WHERE NAME LIKE %:?%");
            preparedStatement.setString(1,keyword);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                tracks.add(new Tracks(
                        resultSet.getString("name"),
                        resultSet.getString("composer")
                ));
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return tracks;
    }

    public Tracks getTrackByName(String name) {
        Tracks track = null;
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT Name, Composer from Track WHERE NAME LIKE ?");
            preparedStatement.setString(1,name);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                track = new Tracks(
                        resultSet.getString("name"),
                        resultSet.getString("composer")
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return track;
    }
}
