package danial.SpringAssignment.data.customer;

import danial.SpringAssignment.data.ConnectionHelper;
import danial.SpringAssignment.models.customer.Customer;
import danial.SpringAssignment.models.customer.CustomerCountry;
import danial.SpringAssignment.models.customer.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Repository
public class CustomerRepo implements CustomerRepoInterface {

    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection connection = null;

    public CustomerRepo() {

    }

    @Override
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            //Class.forName("danial.SpringAssignment.data.Customer.CustomerRepo").newInstance();
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from Customer");
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("customerId"),
                                resultSet.getString("firstname"),
                                resultSet.getString("LastName"),
                                resultSet.getString("country"),
                                resultSet.getString("postalCode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")

                        ));
            }
            System.out.println("Select all customers successful");
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    @Override
    public Customer getCustomerById(String customerId) {
        Customer customer = null;
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM customer WHERE CustomerId = ?");
            preparedStatement.setString(1,customerId);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getString("customerId"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("country"),
                        resultSet.getString("postalCode"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception);;
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception);;            }
        }
        return customer;
    }

    public Customer getCustomerByName(String name) {
        Customer customer = null;
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email from customer WHERE FirstName LIKE ? OR LastName like ? ");
            preparedStatement.setString(1,name);
            preparedStatement.setString(2,name);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getString("customerId"),
                        resultSet.getString("firstName"),
                        resultSet.getString("lastName"),
                        resultSet.getString("country"),
                        resultSet.getString("postalCode"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return customer;
    }

    public ArrayList<Customer> getAmountOfCustomers(int limit, int offset) {
        ArrayList<Customer> customers = new ArrayList<>();
        try{
            //Class.forName("danial.SpringAssignment.data.Customer.CustomerRepo").newInstance();
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId LIMIT ? OFFSET ?");
            // Execute Query
            preparedStatement.setInt(1,limit);
            preparedStatement.setInt(2,offset);


            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("customerId"),
                                resultSet.getString("firstName"),
                                resultSet.getString("lastName"),
                                resultSet.getString("country"),
                                resultSet.getString("postalCode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")

                        ));
            }
            System.out.println("Select all customers successful");
        }
        catch (Exception exception){
            System.out.println(exception);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return customers;
    }

    @Override
    public Boolean addCustomer(Customer customer) {
        Boolean success = false;
        try{
            connection = DriverManager.getConnection(URL);

            // Make SQL query
            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO customer(CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setString(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhone());
            preparedStatement.setString(7,customer.getEmail());


            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Customer added successfully");
        }
        catch (Exception exception){
            System.out.println(exception);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return success;
    }

    @Override
    public Boolean updateCustomer(String customerId, Customer customer) {
        Boolean success = false;
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode= ?, Phone = ?, Email = ? WHERE CustomerId=?");
            preparedStatement.setString(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhone());
            preparedStatement.setString(7,customer.getEmail());
            preparedStatement.setString(8,customerId);
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("Updated customer!");
        }
        catch (Exception exception){
            System.out.println(exception);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return success;
    }

    public ArrayList<CustomerCountry> countCountry() {
        ArrayList<CustomerCountry> customerCountry = new ArrayList<>();
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT Country, COUNT(*) as NUM FROM Customer GROUP BY Country ORDER BY NUM DESC ");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerCountry.add(
                        new CustomerCountry(
                                resultSet.getString("country"),
                                resultSet.getString("NUM")
                        ));
            }
            System.out.println("Select all customer countries successful");

        }
        catch (Exception exception){
            System.out.println(exception);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return customerCountry;
    }

    public ArrayList<CustomerSpender> CustomerSpenders() {
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<>();
        try{
            connection = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT FirstName, LastName,Total FROM Customer AS cust INNER JOIN Invoice AS invo ON cust.CustomerId = invo.CustomerId GROUP BY FirstName ORDER BY max(Total) DESC");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerSpenders.add(
                        new CustomerSpender(
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Total")
                        ));
            }
            System.out.println("Select all customer spenders successful");

        }
        catch (Exception exception){
            System.out.println(exception);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception exception){
                System.out.println(exception);
            }
        }
        return customerSpenders;
    }



}
