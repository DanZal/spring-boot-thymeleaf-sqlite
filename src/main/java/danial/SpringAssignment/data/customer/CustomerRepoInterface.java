package danial.SpringAssignment.data.customer;

import danial.SpringAssignment.models.customer.Customer;
import danial.SpringAssignment.models.customer.CustomerCountry;
import danial.SpringAssignment.models.customer.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepoInterface {
    public ArrayList<Customer> getAllCustomers();
    public Customer getCustomerById(String custId);
    public Customer getCustomerByName(String firstName);
    public Boolean addCustomer(Customer customer);
    public Boolean updateCustomer(String customerid, Customer customer);
    ArrayList<Customer> getAmountOfCustomers(int limit, int offset);
    ArrayList<CustomerCountry> countCountry();
    ArrayList<CustomerSpender> CustomerSpenders();

}
