package danial.SpringAssignment;

import danial.SpringAssignment.data.customer.CustomerRepo;
import danial.SpringAssignment.data.track.TrackRepo;
import danial.SpringAssignment.models.customer.Customer;
import danial.SpringAssignment.models.customer.CustomerCountry;
import danial.SpringAssignment.models.customer.CustomerSpender;
import danial.SpringAssignment.models.track.Tracks;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class SpringAssignmentApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringAssignmentApplication.class, args);

		CustomerRepo customerRepository = new CustomerRepo();
		TrackRepo trackRepo = new TrackRepo();

		ArrayList<Customer> customers = customerRepository.getAllCustomers();
		//Customer danial = new Customer("99", "Danial", "Afzaal", "Norway", "1275", "98874391", "danial@gmail.com");

		//ArrayList<CustomerCountry> customerCountries = customerRepository.countCountry();
		//printCustomerCountries(customerCountries);


		ArrayList<CustomerSpender> customerSpenders = customerRepository.CustomerSpenders();
		//printCustomerSpenders(customerSpenders);

		Tracks track = trackRepo.getTrackByName("once");
		printTrack(track);

		//customerRepository.addCustomer(danial);
		//Customer customer = customerRepository.getCustomerByName("danial");
		//customer.setLastName("Aguero");
		//customerRepository.updateCustomer("Danial", customer);

		//printCustomer(customer);

		//printCustomers(customers);
	}


	public static void printCustomers(ArrayList<Customer> customers) {
		if(customers.size() != 0) {
			for (Customer c : customers) {
				System.out.println("-------------------------------");
				System.out.println(c.getCustomerId());
				System.out.println(c.getFirstName());
				System.out.println(c.getLastName());
			}
		} else {
			System.out.println("No customers returned");
		}
	}

	public static void printCustomerCountries(ArrayList<CustomerCountry> customerCountries) {
		if(customerCountries.size() != 0) {
			for (CustomerCountry c : customerCountries) {
				System.out.println("-------------------------------");
				System.out.println(c.getCountry());
				System.out.println(c.getNumCountry());
			}
		} else {
			System.out.println("No customers countries returned");
		}
	}

	public static void printCustomerSpenders(ArrayList<CustomerSpender> customerSpenders) {
		if(customerSpenders.size() != 0) {
			for (CustomerSpender c : customerSpenders) {
				System.out.println("-------------------------------");
				System.out.println(c.getFirstName() + " " + c.getLastName());
				System.out.println(c.getTotal());
			}
		} else {
			System.out.println("No customers spenders returned");
		}
	}

	public static void printTrack(Tracks track) {

		System.out.println("-------------------------------");
		System.out.println(track.getTrackName());
		System.out.println(track.getComposer());
	}

	public static void printCustomer(Customer customer) {

				System.out.println("-------------------------------");
				System.out.println(customer.getCustomerId());
				System.out.println(customer.getFirstName());
				System.out.println(customer.getLastName());
			}

}
