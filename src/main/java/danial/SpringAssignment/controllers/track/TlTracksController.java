package danial.SpringAssignment.controllers.track;

import danial.SpringAssignment.data.track.TrackRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class TlTracksController {

   TrackRepo trackRepo = new TrackRepo();



    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("tracks", trackRepo.getRandomTracks());
        return "index";
    }

   /* @RequestMapping(value = "/tracks/{name}")
    public String getSpecificTrack(@RequestParam(value="name", defaultValue = "once") String keyword, Model model){
        model.addAttribute("tracks", trackRepo.getRandomTracks());
        if (keyword != null) {
            model.addAttribute("foundTrack", trackRepo.findByKeyword(keyword));
        }
        model.addAttribute("specificTrack", trackRepo.getTrackByName(keyword));
        return "view-customer";
    }*/


    @RequestMapping(value = "/tracks", method = RequestMethod.GET)
    public String showTracksByName(@RequestParam(value = "search", required = false) String name, Model model) {
        model.addAttribute("search", trackRepo.getTrackByName(name));

        return "search-tracks";
    }



}
