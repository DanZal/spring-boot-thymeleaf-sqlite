package danial.SpringAssignment.controllers.customer;

import danial.SpringAssignment.data.customer.CustomerRepo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TlCustomerController {

    private final CustomerRepo customerRepo;

    public TlCustomerController(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }


    /*@RequestMapping(value = "/customers", method = RequestMethod.GET)
    public String getAllCustomers(Model model){
        model.addAttribute("customers", customerRepo.getAllCustomers());
        return "search-results";
    }*/

    @RequestMapping(value = "/customers/{id}")
    public String getSpecificCustomer(@PathVariable String id, Model model){
        model.addAttribute("customer", customerRepo.getCustomerById(id));
        return "search-tracks";
    }

}
