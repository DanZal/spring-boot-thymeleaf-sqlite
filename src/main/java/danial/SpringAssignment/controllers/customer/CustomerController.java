package danial.SpringAssignment.controllers.customer;

import danial.SpringAssignment.data.customer.CustomerRepo;
import danial.SpringAssignment.models.customer.Customer;
import danial.SpringAssignment.models.customer.CustomerCountry;
import danial.SpringAssignment.models.customer.CustomerSpender;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class CustomerController {

    private final CustomerRepo customerRepo;

    public CustomerController(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    @RequestMapping(value = "api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getAllCustomers() { return customerRepo.getAllCustomers(); }

    @RequestMapping(value = "api/customer/id", method = RequestMethod.GET)
    public Customer getCustomerByQueryId(@RequestParam(value="id", defaultValue = "1") String id){
        return customerRepo.getCustomerById(id);
    }

    @RequestMapping(value = "api/customer/name", method = RequestMethod.GET)
    public Customer getCustomerByQueryName(@RequestParam(value="name", defaultValue = "Astrid") String name){
        return customerRepo.getCustomerByName(name);
    }

    //Needs to be done for limit and offset
    @RequestMapping(value = "api/customer/page", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomerAmount(@RequestParam(value="limit", defaultValue = "10") int limit, int offset) {
        return customerRepo.getAmountOfCustomers(limit, offset);
    }

    @RequestMapping(value = "api/customer/country", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getCustomerCountry(){
        return customerRepo.countCountry();
    }

    @RequestMapping(value = "api/customer/spender", method = RequestMethod.GET)
    public ArrayList<CustomerSpender> getCustomerSpender(){
        return customerRepo.CustomerSpenders();
    }
}
