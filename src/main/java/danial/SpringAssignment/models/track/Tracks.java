package danial.SpringAssignment.models.track;

public class Tracks {

    private String trackName;
    private String composer;

    public Tracks(String trackName, String composer) {
        this.trackName = trackName;
        this.composer = composer;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getComposer() {
        return composer;
    }

    public void setComposer(String composer) {
        this.composer = composer;
    }
}
