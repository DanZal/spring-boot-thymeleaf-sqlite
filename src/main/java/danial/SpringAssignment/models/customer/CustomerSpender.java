package danial.SpringAssignment.models.customer;

public class CustomerSpender {
    private String firstName;
    private String lastName;
    private String Total;

    public CustomerSpender(String firstName, String lastName, String total) {
        this.firstName = firstName;
        this.lastName = lastName;
        Total = total;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }
}
