package danial.SpringAssignment.models.customer;

public class CustomerCountry {

    private String country;
    private String numCountry;

    public CustomerCountry(String country, String numCountry) {
        this.country = country;
        this.numCountry = numCountry;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNumCountry() {
        return numCountry;
    }

    public void setNumCountry(String numCountry) {
        this.numCountry = numCountry;
    }
}
